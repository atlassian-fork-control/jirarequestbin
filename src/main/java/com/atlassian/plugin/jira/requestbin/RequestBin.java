package com.atlassian.plugin.jira.requestbin;

import com.google.common.collect.EvictingQueue;
import com.sun.jersey.api.client.ClientResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Our in memory (currently) requestbin component
 */
@Component
public class RequestBin {

    private static final int MAX_INPUT = 1024 * 1024;

    public static class Msg {
        private final LocalDateTime when;
        private final String data;

        Msg(LocalDateTime when, String data) {
            this.when = when;
            this.data = data;
        }

        public String getData() {
            return data;
        }

        public LocalDateTime getWhen() {
            return when;
        }
    }

    public static class Result {
        private final Response.StatusType statusType;
        private final Msg msg;

        Result(Response.StatusType statusType, Msg msg) {
            this.statusType = statusType;
            this.msg = msg;
        }

        public Msg getMsg() {
            return msg;
        }

        public Response.StatusType getStatusType() {
            return statusType;
        }

        public boolean isOK() {
            return statusType.getFamily().equals(Response.Status.Family.SUCCESSFUL);
        }
    }


    private final EvictingQueue<Msg> msgQueue = EvictingQueue.create(20);

    public Result captureMessage(InputStream data) {
        InputStreamReader reader = new InputStreamReader(data);
        StringBuilder sb = new StringBuilder();
        int count = 0;
        int ch;
        try {
            while ((ch = reader.read()) != -1) {
                if (count > MAX_INPUT) {
                    IOUtils.closeQuietly(data);
                    return new Result(ClientResponse.Status.REQUEST_ENTITY_TOO_LARGE, null);
                }
                sb.append((char) ch);
                count++;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Msg msg = new Msg(LocalDateTime.now(), sb.toString());
        msgQueue.add(msg);
        return new Result(Response.Status.OK, msg);
    }


    /**
     * @return the last n messages in reverse order
     */
    public List<Msg> getMessages() {
        List<Msg> msgs = new ArrayList<>(msgQueue);
        Collections.reverse(msgs);
        return msgs;
    }

}
