package com.atlassian.plugin.jira.requestbin.resource;

import com.atlassian.plugin.jira.requestbin.RequestBin;
import com.atlassian.plugins.rest.common.multipart.FilePart;
import com.atlassian.plugins.rest.common.multipart.MultipartFormParam;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.apache.commons.io.HexDump;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 */
@Path("/")
@Produces(MediaType.TEXT_PLAIN)
@AnonymousAllowed
public class RequestBinResource {

    private final RequestBin requestBin;

    public RequestBinResource(RequestBin requestBin) {
        this.requestBin = requestBin;
    }


    @POST
    @Consumes({MediaType.WILDCARD})
    public Response write(InputStream data, @Context HttpHeaders httpHeaders) {
        return captureStream(data);
    }

    @POST
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    public Response writeFormEncoded(@FormParam("data") String data, @Context HttpHeaders httpHeaders) {
        return captureString(data);
    }

    @POST
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    public Response writeFormData(@MultipartFormParam("data") FilePart part, @Context HttpHeaders httpHeaders) throws IOException {
        return captureStream(part.getInputStream());
    }

    private Response captureString(String data) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(new byte[0]);
        if (data != null) {
            inputStream = new ByteArrayInputStream(data.getBytes());
        }
        return captureStream(inputStream);
    }

    private Response captureStream(InputStream data) {
        RequestBin.Result result = requestBin.captureMessage(data);
        if (result.isOK()) {
            return Response.ok().entity(result.getMsg().getData()).build();
        }
        return Response.status(result.getStatusType()).build();
    }


    @GET
    public Response read() {
        StreamingOutput streamingOutput = output -> {
            PrintStream out = new PrintStream(output);

            int count = 0;
            List<RequestBin.Msg> msgs = requestBin.getMessages();
            for (RequestBin.Msg msg : msgs) {
                printMsg(out, msg, count);
                count++;
            }
        };
        return Response.ok(streamingOutput).build();
    }

    private void printMsg(PrintStream out, RequestBin.Msg msg, int count) {
        if (count > 0) {
            out.println();
        }
        out.println("------------------------");
        out.println(String.format("%s #%d length=%d", toDate(msg.getWhen()), count, msg.getData().length()));
        out.println("------------------------");
        dumpMsg(out, msg);
    }

    private String toDate(LocalDateTime when) {
        return DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(when);
    }

    private void dumpMsg(PrintStream out, RequestBin.Msg msg) {
        try {
            String data = msg.getData();
            if (isPrintable(data)) {
                out.print(data);
            } else {
                HexDump.dump(data.getBytes(), 0, out, 0);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean isPrintable(String data) {
        for (int ch : data.toCharArray()) {
            if (!isXmlAllowed(ch)) {
                return false;
            }
        }
        return true;
    }

    private boolean isXmlAllowed(int ch) {
        return (ch == 0x9) ||
                (ch == 0xA) ||
                (ch == 0xD) ||
                ((ch >= 0x20) && (ch <= 0xD7FF)) ||
                ((ch >= 0xE000) && (ch <= 0xFFFD)) ||
                ((ch >= 0x10000) && (ch <= 0x10FFFF));
    }
}
