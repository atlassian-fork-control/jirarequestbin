A simple tool that echos the last few requests send to it much like [https://requestb.in/](https://requestb.in/)



To save a `application/x-www-form-urlencoded` messages do a POST like :

        curl -X POST -d "data=Iwantthiscaptured"  "http://localhost:2990/jira/requestbin"

To save a `multipart/form-data` file do a POST like

        curl -X POST -F "data=@myfileIwantsend.txt"  "http://localhost:2990/jira/requestbin"


To get the last few messages simply do a GET like :

        curl -X GET "http://localhost:2990/jira/requestbin"


NOTE: None of this is secure, in the spirit of `requestbin`, this is a developer / support tool and the size of the messages is limited to 1MB and its
currently keeps 20 of them, evicting the oldest message when this limit is reached.

Since the messages are currently kept in memory
this wont work reliably in a cluster as any one of the nodes may not
be able to serve previously captured requests.  A future version may add "node visible" request
capturing.
